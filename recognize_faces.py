from imutils.video import VideoStream
import face_recognition
import argparse
import imutils
import pickle
import time
import cv2


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--haar-cascade", required=True,
        help = "path to where the face cascade resides")
    parser.add_argument("-f", "--face-encodings", required=True,
        help="path to serialized db of facial encodings")
    parser.add_argument("-i", "--image-file", required=True,
        help="path to input image file for file detection")
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    haar_cascade_path = args.haar_cascade
    face_encodings_filepath = args.face_encodings
    image_file_path = args.image_file

    # Load the known faces and embeddings along with OpenCV's Haar
    # cascade classifier for face detection

    # Load image from image file

    # Convert the input image from BGR to grayscale (for face detection - Haar cascade classifier)
    # and from BGR to RGB (for face recognition - face_recognition package)

    # Detect faces in the grayscale image using Haar cascade classifier

    # Translate OpenCV coordinates from (x, y, w, h) to (top, right, bottom, left)
    # Translation formula: (top, right, bottom, left) = (y, x + w, y + h, x)

    # Compute the facial embeddings for each face using translated coordinates and
    # RGB image. Be aware that face_recognition.face_encodings() function returns
    # returns a list!

    # Loop over the facial embeddings

    #   Attempt to match each face in the input image to our known encodings

    #   Find the indexes of all matched faces then count how many times each person was matched
    #   to detected face - assign their name to the face. If nothing was matched, assign "unknown"
    #   to the face.

    # Loop over the recognized faces
    # boxes are locations of faces detected by Haar classifier and translated
    # to different format and names are the corresponding names assigned in the
    # previous step
    #
    # for ((top, right, bottom, left), name) in zip(boxes, names):

    #   Draw rectangles around the detected faces and display a person's name

    # Display the image to the screen
